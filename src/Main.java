import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();
        double d = sc.nextDouble();

        double x=a+b;
        double y=a+b+c;
        double z=a+b+c+d;

        System.out.printf("%.4f",x);
        System.out.println();
        System.out.printf("%.4f",y);
        System.out.println();
        System.out.printf("%.4f",z);


    }
}